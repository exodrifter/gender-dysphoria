```
  ┏┓     ┓      
  ┃┓┏┓┏┓┏┫┏┓┏┓  
  ┗┛┗ ┛┗┗┻┗ ┛   
┳┓     ┓     •  
┃┃┓┏┏┏┓┣┓┏┓┏┓┓┏┓
┻┛┗┫┛┣┛┛┗┗┛┛ ┗┗┻
   ┛ ┛
```

A short 20 minute non-linear narrative game about negative experiences after
coming out as transgender.

If you like this game, please consider purchasing the game or the OST,
supporting me on Ko-fi or Patreon, or joining my community on Discord.

**Developer's Note:** This is a rather indulgent and unforgiving game, at least
for me, as it only contains negative experiences. I recommend you only consider
playing this game if you have the emotional capacity for it. I hope that it can
give cisgender people an idea of what being transgender is like and for those
who are transgender themselves that they are not alone in their experiences and
that there's someone else who has felt the pain too. But, I also made this game
for myself. I've made this game the same way you might write about the painful,
dark memories you have in a journal to help process it and move on.

Controls:
- Arrow keys/Enter or Mouse
- Press 1 to toggle the dyslexic font
- Press 2 to toggle the descriptive audio

# Installation

Copy the archive file matching your operating system to your computer and unpack
it using the unzip utility of your choice (for Windows, you probably want the
64-bit version).

Then, depending on your operating system, run the following file:
- Linux: `gender-dysphoria.x86_64`
- Windows: `Gender Dysphoria.exe`
- MacOS: `Gender Dysphoria.app`

On MacOS, you'll need to bypass GateKeeper in order to play the game. To do
this, right click on the app, select "Open", and then press "Open" again when
you see the prompt that says this application is from an unidentified developer.
Sorry! Apple makes it expensive and annoying to get anything properly signed for
their devices and I don't have the time and money for it.

If for some reason none of these work, you might still be able to play this game
for free in your web browser: https://exodrifter.itch.io/gender-dysphoria

# Commentary

Thanks for buying a physical copy of _Gender Dysphoria_. I've included this
easter egg at the end as a little bonus for those of you who have bought the
physical copy or have gotten it gifted to you.

I was trying to be a full-time indie developer for the first time in my life at
the time I made this game. I was working on a game called _Access_ and I was
deeply dissatisfied with it, burnt out, extremely depressed, and lonely. So, in
the summer of 2020 I decided to try and find my passion for game development
again by doing a short game jam, just for myself, originally just to prove the
capabilities of the narrative scripting language I had designed. I gave myself
a weekend, 72 hours starting on Friday and ending the following Sunday. On a
spur of the moment, I decided to write about the feelings I had at the time with
transitioning to my preferred gender. By the end of the jam I found that I had
been exhausted emotionally. I planned to write more, but I just couldn't
anymore.

I never thought of the dialog as particularly good. It was incomplete to me and
I didn't really have time to do any editing during the jam and I didn't really
want to afterwards. But, to my surprise, people seemed to connect with it.

My first indie stint ended in failure. I never released _Access_ and I got a
full time job in the software development industry again. But, I kept spending
more time on it because people kept playing it. I continued fixing bugs, I hired
voice actors to record voiceovers, and I added rudimentary support for people
with vision handicaps. _Gender Dysphoria_ got published in Indiepocalypse, in
the Queer Games Bundle of 2021 and 2022, and I eventually got it on Steam too.
Before I knew it, I spent a ridiculous amount of time and money on what was
essentially a 20 minute novel that I had written in 72 hours and had most of its
pages shuffled in a different order every time you read it.

It was a lot of work, but I was happy to do it. I heard stories from friends and
strangers alike who have been changed or touched by playing _Gender Dysphoria_
and I couldn't be more thankful for everyone who has sent me kind words about
it. I hope this short game earns a special place in your heart too, as I really
desire to create meaningful, emotional experiences for people to have.

I'm trying full-time independent game dev again for the second time now as I
write this. Hopefully, I can find a way to give everyone another meaningful,
emotional experience again -- this time hopefully with dialog that I think is
better and takes longer than 72 hours to produce -- and find a way to keep doing
it full time without compromising on my values. One can hope.

Love,
Ava Pek (exodrifter)
https://www.exodrifter.space
2024-02-12
