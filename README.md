# Gender Dysphoria

A short 10-15 minute non-linear narrative game about negative experiences after
coming out as transgender.

Play it on [itch.io][1] or [Steam][2].

[1]: https://exodrifter.itch.io/gender-dysphoria
[2]: https://store.steampowered.com/app/2310400/Gender_Dysphoria/
