﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class PressNextScene : MonoBehaviour
{
	[SerializeField]
	private string nextScene = default;

	public void NextScene()
	{
		SceneManager.LoadScene(nextScene);
	}
}
