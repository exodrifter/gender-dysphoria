﻿using UnityEngine;
using UnityEngine.UI;

public class Credits : MonoBehaviour
{
	[SerializeField]
	private TextAsset credits = default;

	void Awake()
	{
		GetComponent<Text>().text = credits.text;
	}
}
