using UnityEngine;

public class PressEndGame : MonoBehaviour
{
	public void Awake()
	{
		if (Application.platform == RuntimePlatform.WebGLPlayer)
		{
			Destroy(gameObject);
		}
	}

	public void EndGame()
	{
		Application.Quit();
	}
}
