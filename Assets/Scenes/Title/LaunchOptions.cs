using System.Linq;
using UnityEngine;

public class LaunchOptions : MonoBehaviour
{
	void Awake()
	{
		var args = System.Environment.GetCommandLineArgs();
		var turnOnAllAccessibility = args.Contains("--enable-all-accessibility");

		GameState.descriptive = turnOnAllAccessibility;
		GameState.dyslexic = turnOnAllAccessibility;
	}
}
