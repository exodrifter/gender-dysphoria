using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Users;
using UnityEngine.UI;

public class HintText : MonoBehaviour
{
	private Text text;
	public InputActionAsset inputActions;
	public PlayerInput input;

	void Start()
	{
		InputUser.onChange += OnChange;
		text = GetComponent<Text>();
		SetHints();
	}

	void OnDestroy()
	{
		InputUser.onChange -= OnChange;
	}

	void OnChange(InputUser user, InputUserChange change, InputDevice device)
	{
		switch (change) {
			case InputUserChange.ControlSchemeChanged:
				SetHints();
				break;
		}
	}

	private void SetHints()
	{
		var actionMap = inputActions.FindActionMap("UI");
		var dyslexic = actionMap["ToggleDyslexic"].GetBindingDisplayString();
		var descriptive = actionMap["ToggleDescriptive"].GetBindingDisplayString();
		text.text = $"Press <b>{dyslexic}</b> to toggle dyslexic font\n";
		text.text += $"Press <b>{descriptive}</b> to toggle descriptive audio";
	}
}
