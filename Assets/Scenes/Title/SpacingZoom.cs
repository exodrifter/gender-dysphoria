﻿using Exodrifter.Anchor;
using Exodrifter.Aural;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class SpacingZoom : MonoBehaviour
{
	private LetterShift letterShift;
	private Text text;

	private int? glitchIndex;
	private float volume;
	private float t;
	private Font originalFont;
	private int originalFontSize;
	private float originalSpacing;

	[SerializeField]
	private VerticalTitle verticalTitle = default;
	[SerializeField]
	private Soundbank glitchSounds = default;
	[SerializeField, Range(0, 1)]
	private float maxPan = default;
	[SerializeField]
	private Font[] glitchFonts = default;
	[SerializeField]
	private int[] glitchFontSizes = default;
	[SerializeField]
	private float[] glitchSpacing = default;


	private IEnumerator ShuffleSeed()
	{
		while (true)
		{
			yield return new WaitForSeconds(Random.Range(1, 4));

			int i = Random.Range(2, 5);
			while (i > 0)
			{
				letterShift.seed = Random.Range(int.MinValue, int.MaxValue);
				i--;

				if (verticalTitle?.Visible ?? true)
				{
					var param = TransientParams.Variable2D()
						.PanStereo(FloatVariance.Range(-maxPan, maxPan))
						.Volume(FloatVariance.Variance(volume, 0.1f))
						.Pitch(FloatVariance.Range(0.5f, 1.5f));
					Audio.Hit(glitchSounds, param);
				}

				if (glitchFonts.Length > 0)
				{
					glitchIndex = Random.Range(0, glitchFonts.Length);
				}

				yield return new WaitForSeconds(Random.Range(0f, 0.1f));
			}

			glitchIndex = null;
		}
	}

	private void Awake()
	{
		letterShift = GetComponent<LetterShift>();
		text = GetComponent<Text>();

		glitchIndex = null;
		volume = 0;
		t = 0;
		originalFont = text.font;
		originalFontSize = text.fontSize;
		originalSpacing = letterShift.spacing;

		StartCoroutine(ShuffleSeed());
	}

	private IEnumerator Start()
	{
		var elapsed = 0f;
		var time = 20f;

		while (elapsed < time)
		{
			yield return new WaitForEndOfFrame();
			elapsed += Time.deltaTime;
			t = Easing.EaseInOutQuad.Interpolate(elapsed / time);
		}

		t = 1;
	}

	private void Update()
	{
		volume = t * 0.8f;

		if (glitchIndex.HasValue && t > 0.2f)
		{
			var i = glitchIndex.Value;
			text.font = glitchFonts[i];
			text.fontSize = glitchFontSizes[i];
			letterShift.spacing = Mathf.Lerp(0, glitchSpacing[i], t);
		}
		else
		{
			text.font = originalFont;
			text.fontSize = originalFontSize;
			letterShift.spacing = Mathf.Lerp(0, originalSpacing, t);
		}
	}
}
