using UnityEngine;

public class TitleButton : XButton
{
	protected override Color OnColor { get { return Color.cyan; } }
	protected override Color OnTextColor { get { return Color.black; } }

	protected override Color OffColor { get { return Color.white; } }
	protected override Color OffTextColor { get { return Color.black; } }

	protected override Color DisabledColor { get { return Color.grey; } }
	protected override Color DisabledTextColor { get { return Color.black; } }
}
