﻿using UnityEngine;

public class VerticalTitle : MonoBehaviour
{
	[SerializeField]
	private bool useWhenVertical = false;

	public bool Visible
	{
		get
		{
			float r = Screen.height / (float)Screen.width;
			float boundary = 0.65f;

			if (useWhenVertical)
			{
				return r > boundary;
			}
			else
			{
				return r <= boundary;
			}
		}
	}

	private CanvasGroup group;

	private void Awake()
	{
		group = GetComponent<CanvasGroup>();
	}

	private void Update()
	{
		if (Visible)
		{
			group.alpha = 1;
		}
		else
		{
			group.alpha = 0;
		}
	}
}
