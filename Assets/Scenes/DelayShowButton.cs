using System.Collections;
using Exodrifter.Aural;
using UnityEngine;

/// <summary>
/// Hides a button until either:
/// * The descriptive audio is finished
/// * A certain amount of time has passed
/// </summary>
public class DelayShowButton : MonoBehaviour
{
	[SerializeField]
	private XButton button;

	// The descriptive audio to listen to before the button can be shown.
	[SerializeField]
	private AudioClip descriptive;

	// The minimum amount of time that must be waited before the button can be
	// shown if the user does not listen to the descriptive audio.
	[SerializeField]
	private float delay;

	private bool oldDescriptive;

	void Awake()
	{
		oldDescriptive = GameState.descriptive;
		if (oldDescriptive)
		{
			StartCoroutine(DescribeThenShow());
		}
		else
		{
			StartCoroutine(DelayThenShow());
		}
	}

	void Update()
	{
		delay -= Time.deltaTime;
		if (delay < 0)
		{
			delay = 0;
		}

		if (GameState.descriptive)
		{
			// If the descriptive setting was just turned on, play the
			// descriptive audio then show the button.
			if (!oldDescriptive)
			{
				StopAllCoroutines();
				StartCoroutine(DescribeThenShow());
			}
		}
		else
		{
			// If the descriptive setting was just turned off, delay (for the
			// remaining amount of time) then show the button.
			if (oldDescriptive)
			{
				StopAllCoroutines();
				StartCoroutine(DelayThenShow());
			}
		}

		oldDescriptive = GameState.descriptive;
	}

	private IEnumerator DelayThenShow()
	{
		Audio.Halt(descriptive);

		if (delay > 0)
		{
			Hide();
			yield return new WaitForSeconds(delay);
			Show();
		}
		else
		{
			Show();
		}
	}

	private IEnumerator DescribeThenShow()
	{
		Audio.Halt(descriptive);

		if (descriptive == null)
		{
			yield return DelayThenShow();
		}
		else
		{
			Hide();
			Audio.Hit(descriptive, TransientParams.Const2D());
			yield return new WaitForSeconds(descriptive.length + 1);
			Show();
		}
	}

	private void Hide()
	{
		button.GetComponent<CanvasGroup>().alpha = 0;
		button.Disabled = true;
	}

	private void Show()
	{
		button.GetComponent<CanvasGroup>().alpha = 1;
		button.Disabled = false;
		button.TemporaryDisable();
	}
}
