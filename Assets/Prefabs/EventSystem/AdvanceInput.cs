using Exodrifter.Anchor;
using UnityEngine;
using UnityEngine.EventSystems;
using static UnityEngine.InputSystem.InputAction;

public class AdvanceInput : MonoBehaviour
{
	public static void Advance(CallbackContext ctx)
	{
		if (!ctx.started)
		{
			return;
		}

		if (Util.IsNull(EventSystem.current.currentSelectedGameObject))
		{
			var conversation = FindObjectOfType<Conversation>();
			conversation?.Advance();
		}
	}
}
