using UnityEngine;

public class AccessibilityShortcuts : MonoBehaviour
{
	public void ToggleDyslexic()
	{
		GameState.dyslexic = !GameState.dyslexic;
	}

	public void ToggleDescriptive()
	{
		GameState.descriptive = !GameState.descriptive;
	}
}
