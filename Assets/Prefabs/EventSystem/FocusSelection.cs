using Exodrifter.Anchor;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

/// <summary>
/// Focuses the selection on the first available navigatable GUI element if none
/// is currently focused.
/// </summary>
public class FocusSelection : MonoBehaviour
{
	public static void Focus()
	{
		if (Util.IsNull(EventSystem.current.currentSelectedGameObject))
		{
			var selectables = FindObjectsOfType<Selectable>();

			// Find the "top-most" selectable
			Selectable selectable = null;
			int i = int.MaxValue;
			foreach (var s in selectables)
			{
				if (s.transform.GetSiblingIndex() < i)
				{
					selectable = s;
					i = s.transform.GetSiblingIndex();
				}
			}

			if (!Util.IsNull(selectable))
			{
				EventSystem.current.SetSelectedGameObject(selectable.gameObject);
			}
		}
	}
}
