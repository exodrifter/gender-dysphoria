﻿using Exodrifter.Aural;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class StopPreviousAudio : MonoBehaviour
{
	[SerializeField]
	private bool halt = false;

	public void Awake()
	{
		if (halt)
		{
			Audio.HaltAllExcept(GetUsedAudioClips(), GetUsedSoundbanks());
		}
		else
		{
			Audio.StopAllExcept(GetUsedAudioClips(), GetUsedSoundbanks());
		}
	}

	public IEnumerable<AudioClip> GetUsedAudioClips()
	{
		return FindObjectsOfType<AudioClipHitPlayer>().Select(x => x.Clip)
			.Concat(FindObjectsOfType<AudioClipLoopPlayer>().Select(x => x.Clip));
	}

	public IEnumerable<Soundbank> GetUsedSoundbanks()
	{
		return FindObjectsOfType<SoundbankLoopPlayer>().Select(x => x.Soundbank);
	}
}
