﻿using Exodrifter.Anchor;
using Exodrifter.Aural;
using Exodrifter.Rumor.Compiler;
using Exodrifter.Rumor.Engine;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Conversation : MonoBehaviour
{
	[SerializeField]
	private RumorPack pack = default;
	[SerializeField]
	private string nextScene = default;

	[SerializeField]
	private AudioMixerGroup uiMixerGroup = default;

	[SerializeField]
	private AudioClip blip = default;
	private const float BLIP_VOLUME = 0.1f;
	[SerializeField]
	private AudioClip nextSceneSound = default;

	[SerializeField]
	private Pool leftPool = default;
	[SerializeField]
	private Pool rightPool = default;
	[SerializeField]
	private Pool centerPool = default;
	[SerializeField]
	private Pool choicePool = default;

	private Advance advance;
	private Rumor rumor;

	private AudioClip lastDialog;

	void Start()
	{
		advance = GetComponentInChildren<Advance>();

		rumor = new Rumor(
			new RumorCompiler()
				.LinkAction("hit", ValueType.String)
				.Compile(pack.Script.text),
			GameState.Scope
		);

		rumor.State.OnSetDialog += OnSetDialog;
		rumor.State.OnAppendDialog += OnAddDialog;
		rumor.State.OnClear += OnClear;
		rumor.OnWaitForChoose += OnWaitForChoose;
		rumor.OnWaitForAdvance += advance.MakeVisible;

		rumor.Bindings.Bind<string>("hit", name => {
				Audio.Halt(lastDialog);
				lastDialog = pack.GetAudio(name);
				Audio.Hit(lastDialog, TransientParams.Const2D());
			}
		);

		rumor.OnFinish += () =>
		{
			if (!string.IsNullOrEmpty(nextScene))
			{
				SceneManager.LoadScene(nextScene);
			}
			else
			{
				var scene = GameState.GetNextScene();

				if (string.IsNullOrEmpty(scene))
				{
					SceneManager.LoadScene("Phone 5");
				}
				else
				{
					SceneManager.LoadScene(scene);
				}
			}

			Audio.Hit(nextSceneSound, TransientParams.Const2D());
		};

		rumor.Start();
	}

	private void OnClear(ClearType type)
	{
		switch (type)
		{
			case ClearType.All:
				leftPool.DespawnAll();
				rightPool.DespawnAll();
				centerPool.DespawnAll();
				choicePool.DespawnAll();
				break;

			case ClearType.Choices:
				choicePool.DespawnAll();
				break;

			case ClearType.Dialog:
				leftPool.DespawnAll();
				rightPool.DespawnAll();
				centerPool.DespawnAll();
				break;
		}
	}

	private void OnWaitForChoose(Dictionary<string, string> choices)
	{
		// If we're in descriptive mode, then halt the last dialog
		if (GameState.descriptive)
		{
			Audio.Halt(lastDialog);
		}

		Selectable previous = null;

		var index = 0;
		foreach (var choice in choices)
		{
			var button = choicePool.Spawn<Button>();
			button.name = choice.Value;

			// Setup descriptive audio
			var xButton = button.GetComponent<XButton>();
			xButton.description = pack.GetAudio(choice.Key);

			// Setup input handling
			button.onClick.RemoveAllListeners();
			button.onClick.AddListener(() =>
				{
					foreach (var xb in FindObjectsOfType<XButton>())
					{
						xb.HaltAudio();
					}

					rumor.Choose(choice.Key);
					EventSystem.current.SetSelectedGameObject(null);
				}
			);

			// Setup navigation
			if (previous != null)
			{
				var prevNav = previous.navigation;
				prevNav.mode = Navigation.Mode.Explicit;
				prevNav.selectOnDown = button;
				previous.navigation = prevNav;
			}

			var nav = button.navigation;
			nav.mode = Navigation.Mode.Explicit;
			nav.selectOnUp = previous;
			button.navigation = nav;

			// Set up text
			var text = button.GetComponentInChildren<Text>();
			text.text = choice.Value;

			button.transform.SetAsLastSibling();

			previous = button;
			index++;
		}

		if (previous != null)
		{
			var prevNav = previous.navigation;
			prevNav.mode = Navigation.Mode.Explicit;
			prevNav.selectOnDown = null;
			previous.navigation = prevNav;
		}
	}

	private void OnSetDialog(object speaker, string dialog)
	{
		switch (speaker)
		{
			case "me":
				OnSetDialog(leftPool, speaker, dialog);
				break;

			case "_narrator":
				OnSetDialog(centerPool, speaker, dialog);
				break;

			default:
				OnSetDialog(rightPool, speaker, dialog);
				break;
		}

		var param = TransientParams.Const2D(BLIP_VOLUME)
			.MixerGroup(uiMixerGroup);
		Audio.Hit(blip, param);
	}

	private void OnSetDialog(Pool pool, object speaker, string dialog)
	{
		var go = pool.Spawn();
		go.GetComponentInChildren<Text>().text = dialog;
		go.transform.SetAsLastSibling();
	}

	private void OnAddDialog(object speaker, string dialog)
	{
		switch (speaker)
		{
			case "me":
				OnAddDialog(leftPool, speaker, dialog);
				break;

			case "_narrator":
				OnAddDialog(centerPool, speaker, dialog);
				break;

			default:
				OnAddDialog(rightPool, speaker, dialog);
				break;
		}

		var param = TransientParams.Const2D(BLIP_VOLUME)
			.MixerGroup(uiMixerGroup);
		Audio.Hit(blip, param);
	}

	public void OnAddDialog(Pool pool, object speaker, string dialog)
	{
		var go = pool.Spawned.LastOrDefault();
		if (go == null)
		{
			OnSetDialog(speaker, dialog);
		}
		else
		{
			go.GetComponentInChildren<Text>().text += " " + dialog;
			go.transform.SetAsLastSibling();
		}
	}

	void Update()
	{
		rumor?.Update(Time.deltaTime);
	}

	public void Advance()
	{
		rumor?.Advance();
	}
}
