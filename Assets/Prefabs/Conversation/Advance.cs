﻿using UnityEngine;

public class Advance : MonoBehaviour
{
	private Conversation conversation;
	private CanvasGroup group;

	private bool visible;
	private float visibleFor;

	private void Awake()
	{
		conversation = GetComponentInParent<Conversation>();
		group = GetComponent<CanvasGroup>();
		group.alpha = 0;
	}

	public void MakeVisible()
	{
		visible = true;
	}

	void Update()
	{
		if (visible)
		{
			visibleFor += Time.deltaTime;
		}
		else
		{
			visibleFor = 0;
		}

		group.alpha = visible ? 1 : 0;
	}

	public void OnSubmit()
	{
		if (visible && visibleFor > 0.5f)
		{
			visible = false;
			visibleFor = 0;
			conversation.Advance();
		}
	}

	private void LateUpdate()
	{
		transform.SetAsLastSibling();
	}
}
