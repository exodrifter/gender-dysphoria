using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Users;

public class HideCursor : MonoBehaviour
{
	private static bool? usingGamepad = null;

	private PlayerInput input;

	void Awake()
	{
		input = FindObjectOfType<PlayerInput>();
		InputUser.onChange += OnChange;

		// Initialize static controller state
		if (usingGamepad == null)
		{
			// Default to showing gamepad controls if it is connected
			usingGamepad = Gamepad.all.Count > 0;
		}

		// Remind the input user what device we want to use
		if (usingGamepad == true)
		{
			input.SwitchCurrentControlScheme(Gamepad.current);
			SetCursorVisibility();
		}
	}

	void OnDestroy()
	{
		InputUser.onChange -= OnChange;
	}

	void OnChange(InputUser user, InputUserChange change, InputDevice device)
	{
		switch (change)
		{
			case InputUserChange.ControlSchemeChanged:
				usingGamepad = UsingGamepad(input);
				SetCursorVisibility();
				break;
		}
	}

	private static void SetCursorVisibility()
	{
		Cursor.visible = (usingGamepad ?? false) == false;
	}

	private static bool UsingGamepad(PlayerInput input)
	{
		foreach (var device in input.devices)
		{
			if (device == Gamepad.current)
			{
				return true;
			}
		}
		return false;
	}
}
