using Exodrifter.Rumor.Compiler;
using Exodrifter.Rumor.Engine;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using UnityEditor;
using UnityEngine;

public class Tools : MonoBehaviour
{
	/// <summary>
	/// This shortcut should be manually used when we need to update the
	/// https://gitlab.com/exodrifter/access/-/wikis/script-links page.
	/// </summary>
	[MenuItem("Tools/Wiki/Generate Links")]
	private static void GenerateLinks()
	{
		var textAssets = AssetDatabase.GetAllAssetPaths()
			.Where(x => x.EndsWith(".rumor"))
			.OrderBy(x => x);

		var currentLevel = "";
		var markdown = new StringBuilder();
		var wordsTotal = new Dictionary<string, int>();
		var linesTotal = new Dictionary<string, int>();

		markdown.AppendLine("\n# Levels");
		foreach (var asset in textAssets)
		{
			var currentFolder = Directory.GetParent(Directory.GetParent(asset).FullName).Name;
			if (currentLevel != currentFolder)
			{
				currentLevel = currentFolder;
				markdown.AppendLine("\n## " + currentLevel);
			}

			var text =
				AssetDatabase.LoadAssetAtPath(asset, typeof(TextAsset))
					as TextAsset;
			var nodes = new RumorCompiler()
				.LinkAction("hit", ValueType.String)
				.Compile(text.text);

			var words = new Dictionary<string, int>();
			var lines = new Dictionary<string, int>();
			foreach (var section in nodes)
			{
				foreach (var node in section.Value)
				{
					if (node is DialogNode)
					{
						var n = (DialogNode)node;

						var s = n.Speaker ?? "_narrator";
						var d = n.Dialog.Evaluate(new RumorScope())
							.AsString().Value;
						if (words.ContainsKey(s))
						{
							words[s] += d.Split(' ').Length;
							lines[s] += 1;
						}
						else
						{
							words[s] = d.Split(' ').Length;
							lines[s] = 1;
						}
					}
					else if (node is AddChoiceNode)
					{
						var n = (AddChoiceNode)node;
						var d = n.Text.Evaluate(new RumorScope())
							.AsString().Value;
						if (words.ContainsKey("_system"))
						{
							words["_system"] += d.Split(' ').Length;
							lines["_system"] += 1;
						}
						else
						{
							words["_system"] = d.Split(' ').Length;
							lines["_system"] = 1;
						}
					}
				}
			}

			markdown.AppendLine("* " + Link(Path.GetFileName(asset), asset));

			foreach (var x in words)
			{
				markdown.AppendLine(
					"  * `" + x.Key + "`: " + x.Value + " words; " +
					lines[x.Key] + " lines"
				);

				if (wordsTotal.ContainsKey(x.Key))
				{
					wordsTotal[x.Key] += x.Value;
					linesTotal[x.Key] += lines[x.Key];
				}
				else
				{
					wordsTotal[x.Key] = x.Value;
					linesTotal[x.Key] = lines[x.Key];
				}
			}
		}

		markdown.AppendLine("\n# Characters");
		foreach (var x in wordsTotal)
		{
			markdown.AppendLine(
				"* `" + x.Key + "`: " + x.Value + " total words; " +
				linesTotal[x.Key] + " total lines"
			);
		}


		var path = Path.Combine(Application.persistentDataPath, "script-links.md");
		File.WriteAllText(path, markdown.ToString().TrimStart());
		UnityEngine.Debug.Log("Markdown written to " + path);

		Process.Start(path);
	}

	private static string Link(string name, string path)
	{
		var baseUrl = "https://gitlab.com/exodrifter/gender-dysphoria/blob/master/";
		var escapedPath = path.Replace(" ", "%20");
		return "[" + name + "](" + baseUrl + escapedPath + ")";
	}
}
