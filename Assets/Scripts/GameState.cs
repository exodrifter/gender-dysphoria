﻿using Exodrifter.Rumor.Engine;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameState
{
	public static bool dyslexic = false;
	public static bool descriptive = false;
	private static List<string> scenes;
	public static RumorScope Scope { get; private set; } = new RumorScope();

	[RuntimeInitializeOnLoadMethod]
	public static void NewGame()
	{
		Scope = new RumorScope();
		scenes = ShuffleStoryArcs(
			new List<string>()
			{
				"Phone 2",
				"Phone 3",
				"Phone 4",
			},
			new List<string>()
			{
				"Pro Bono 1",
				"Pro Bono 2",
				"Pro Bono 3",
				"Pro Bono 4",
				"Pro Bono 5",
			},
			new List<string>()
			{
				"Networking 1",
				"Networking 2",
				"Networking 3",
				"Networking 4",
				"Networking 5",
			},
			new List<string>()
			{
				"Shopping 1",
				"Shopping 2",
				"Shopping 3",
				"Shopping 4",
				"Shopping 5",
			}
		);

		SceneManager.sceneLoaded += OnSceneLoaded;
	}

	private static List<string> ShuffleStoryArcs(List<string> phone, params List<string>[] arcs)
	{
		// If there are no arcs, there are no scenes to shuffle!
		if (arcs.Length == 0)
		{
			return new List<string>();
		}

		var scenes = new List<string>();

		string lastArcPrefix = null;

		// Continue until there are no more scenes from each arc.
		while (arcs.Select(x => x.Count).Max() > 0)
		{
			// Randomize the order of the arcs.
			arcs = arcs.OrderBy(_ => Random.value).ToArray();

			// Make sure scenes in the same arc do not get shown back to back.
			var thisArcPrefix = GetArcPrefix(arcs[0]);
			if (thisArcPrefix != null && lastArcPrefix != null)
			{
				if (thisArcPrefix == lastArcPrefix)
				{
					arcs = arcs.Reverse().ToArray();
				}
			}
			lastArcPrefix = GetArcPrefix(arcs.Last());

			// Add a scene from each of the arcs.
			foreach (var arc in arcs)
			{
				if (arc.Count > 0)
				{
					var index = Random.Range(0, arc.Count);
					scenes.Add(arc[index]);
					arc.RemoveAt(index);
				}
			}

			// Add a phone scene, if possible.
			if (phone.Count > 0)
			{
				var i = Random.Range(0, phone.Count);
				scenes.Add(phone[i]);
				phone.RemoveAt(i);
			}
		}

		return scenes;
	}

	private static string GetArcPrefix(List<string> arc)
	{
		if (arc.Count == 0)
		{
			return null;
		}

		var parts = new List<string>(arc[0].Split(' '));
		if (parts.Count == 0)
		{
			return null;
		}
		else
		{
			parts.RemoveAt(parts.Count - 1);
			return string.Join(" ", parts.ToArray());
		}
	}

	private static void OnSceneLoaded(Scene scene, LoadSceneMode arg1)
	{
		scenes.Remove(scene.name);
	}

	public static string GetNextScene()
	{
		if (scenes != null && scenes.Count > 0)
		{
			return scenes[0];
		}

		return null;
	}
}
