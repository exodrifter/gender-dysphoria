using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DyslexicFont : MonoBehaviour
{
	private Text text;
	private int originalSize;

	[SerializeField]
	public Font normalFont;
	[SerializeField]
	public Font dyslexicFont;

	private void Awake()
	{
		text = GetComponent<Text>();
		originalSize = text.fontSize;
	}

	private void Update()
	{
		if (GameState.dyslexic)
		{
			text.font = dyslexicFont;
			text.fontSize = (int)(0.8f * originalSize);
		}
		else
		{
			text.font = normalFont;
			text.fontSize = originalSize;
		}
	}
}
