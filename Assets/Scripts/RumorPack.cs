using UnityEngine;

[CreateAssetMenu(menuName = "Rumor Pack", fileName = "Rumor Pack", order = 100)]
public class RumorPack : ScriptableObject
{
	public TextAsset Script
	{
		get { return script; }
	}
	[SerializeField]
	private TextAsset script;

	[SerializeField]
	private AudioClip[] clips;

	public AudioClip GetAudio(string name)
	{
		foreach (var clip in clips)
		{
			if (clip.name == name)
			{
				return clip;
			}
		}

		return null;
	}
}
