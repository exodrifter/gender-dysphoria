﻿using Exodrifter.Anchor;
using Exodrifter.Aural;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public abstract class XButton : MonoBehaviour,
	IPointerEnterHandler, IPointerClickHandler
{
	[SerializeField]
	private AudioMixerGroup mixerGroup = default;
	[SerializeField]
	private AudioClip hoverClip = default;
	[SerializeField]
	private AudioClip submitClip = default;

	private Text[] texts = default;
	private Image image = default;
	private Button button = default;
	private CanvasGroup canvasGroup = default;

	public AudioClip description;
	public AudioClip buttonClip;

	protected virtual Color OnColor { get { return Color.black; } }
	protected virtual Color OnTextColor { get { return Color.white; } }

	protected virtual Color OffColor { get { return Color.clear; } }
	protected virtual Color OffTextColor { get { return Color.black; } }

	protected virtual Color DisabledColor { get { return Color.clear; } }
	protected virtual Color DisabledTextColor { get { return Color.grey; } }

	protected virtual float DisabledTime { get { return 0.5f; } }

	private enum ButtonState { On, Off, Disabled }

	private ButtonState State
	{
		get
		{
			if (Disabled)
			{
				return ButtonState.Disabled;
			}
			else if (Hovering)
			{
				return ButtonState.On;
			}
			return ButtonState.Off;
		}
	}

	public bool Disabled
	{
		get { return disabled || temporarilyDisabled; }
		set { disabled = value; Repaint(); }
	}
	private bool disabled = false;

	private bool temporarilyDisabled = true;

	public bool Hovering { get; private set; }

	private void Repaint()
	{
		texts = texts ?? GetComponentsInChildren<Text>();
		foreach (var text in (texts ?? new Text[0]))
		{
			switch (State)
			{
				case ButtonState.Disabled:
					text.color = DisabledTextColor;
					break;
				case ButtonState.On:
					text.color = OnTextColor;
					break;
				case ButtonState.Off:
					text.color = OffTextColor;
					break;
			}
		}

		image = image ?? GetComponentInChildren<Image>();
		if (!Util.IsNull(image))
		{
			switch (State)
			{
				case ButtonState.Disabled:
					image.color = DisabledColor;
					break;
				case ButtonState.On:
					image.color = OnColor;
					break;
				case ButtonState.Off:
					image.color = OffColor;
					break;
			}
		}

		canvasGroup = canvasGroup ?? GetComponentInChildren<CanvasGroup>();
		if (!Util.IsNull(canvasGroup))
		{
			canvasGroup.interactable = !Disabled;
			canvasGroup.blocksRaycasts = !Disabled;
		}

		button = button ?? GetComponentInChildren<Button>();
		button.interactable = !Disabled;
		OnRepaint();
	}

	protected virtual void OnRepaint() { }

	private bool halted = false;
	private bool oldDescriptive = false;

	private void LateUpdate()
	{
		// Update hover state
		if (!Disabled)
		{
			var oldHover = Hovering;
			var newHover = EventSystem.current.currentSelectedGameObject == gameObject;
			Hovering = newHover;

			var oldDescriptive = this.oldDescriptive;
			var newDescriptive = GameState.descriptive;
			this.oldDescriptive = newDescriptive;

			// Trigger descriptive audio
			if (GameState.descriptive && description != null)
			{
				if (newHover && (!oldHover || newDescriptive && !oldDescriptive))
				{
					halted = false;
					Audio.Hit(description, TransientParams.Const2D());
					Routine.Start(description.length + 0.1f, _ => {}, () =>
					{
						if (!halted)
						{
							Audio.Hit(buttonClip, TransientParams.Const2D());
						}
					});
				}
				
				if (!newHover && oldHover)
				{
					halted = true;
					HaltAudio();
				}
			}

			if (!GameState.descriptive)
			{
				halted = true;
				HaltAudio();
			}
		}

		// Repaint the button
		Repaint();
	}

	private void OnEnable()
	{
		TemporaryDisable();

		// Reset state
		Hovering = false;
		oldDescriptive = false;
	}

	/// <summary>
	/// Temporarily disables the button. Should be used when the button
	/// appears for the first time or when an action is taken that causes
	/// the UI to change its layout to prevent users from accidentally
	/// clicking something they didn't mean to click.
	/// </summary>
	public void TemporaryDisable()
	{
		temporarilyDisabled = true;
		Repaint();

		Routine.Start(DisabledTime,
			(_) => { temporarilyDisabled = true; },
			() => {
				FocusSelection.Focus();
				temporarilyDisabled = false;
			}
		);
	}

	/// <summary>
	/// Temporarily disables all buttons. This function is not static so it
	/// can be called by a UnityAction.
	/// </summary>
	public void TemporaryDisableAll()
	{
		foreach (var button in FindObjectsOfType<XButton>())
		{
			button.TemporaryDisable();
		}
	}

	public void OnPointerEnter(PointerEventData eventData)
	{
		Audio.Hit(hoverClip, TransientParams.Const2D().MixerGroup(mixerGroup));
		EventSystem.current.SetSelectedGameObject(gameObject, eventData);
	}

	public void OnPointerClick(PointerEventData eventData)
	{
		if (!Disabled && eventData.button == 0)
		{
			Audio.Hit(
				submitClip,
				TransientParams.Const2D().MixerGroup(mixerGroup)
			);
		}
	}

	public void HaltAudio()
	{
		Audio.Halt(description);
		Audio.Halt(buttonClip);
	}
}
